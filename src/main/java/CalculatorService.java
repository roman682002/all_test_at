public interface CalculatorService {
    void readTwoDigitsAndMultiply();
    void readTwoDigitsAndDivide();
    void readTwoDigitsAndSubtract();
    void readTwoDigitsAndSum();
}