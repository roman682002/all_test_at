public class Calculator {

    public static void main(String[] args) {
        IOService ConsoleIOService = new Console();

        CalculatorService calculatorService = new CalculatorServiceImpl(ConsoleIOService);

        // invitation
        ConsoleIOService.out("Enter 2 values to multiply!");

        // silently takes 2 values and multiplies
        calculatorService.readTwoDigitsAndMultiply();
        ConsoleIOService.out("---------------------------------------------------------");

        // invitation
        ConsoleIOService.out("Enter 2 values to divide!");

        // silently takes 2 values and divides
        calculatorService.readTwoDigitsAndDivide();
        ConsoleIOService.out("---------------------------------------------------------");

        // invitation
        ConsoleIOService.out("Enter 2 values to subtract!");

        // silently takes 2 values and multiplies
        calculatorService.readTwoDigitsAndSubtract();
        ConsoleIOService.out("---------------------------------------------------------");

        // invitation
        ConsoleIOService.out("Enter 2 values to add up!");

        // silently takes 2 values and summarizes
        calculatorService.readTwoDigitsAndSum();
        ConsoleIOService.out("---------------------------------------------------------");

    }
}
