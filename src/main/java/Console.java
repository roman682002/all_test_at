import java.io.PrintStream;
import java.util.Scanner;

// closed system I/O class
public class Console implements IOService{

    private final PrintStream out;
    private final Scanner sc;


    public Console() {
        this.out = System.out;
        this.sc = new Scanner(System.in);
    }

    @Override
    public void out(String message) {
        out.println(message);
    }

    @Override // reads a line
    public String readString() {
        return sc.nextLine();
    }
}
