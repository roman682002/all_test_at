import java.io.PrintStream;
import java.util.Scanner;

public class CalculatorServiceImpl implements CalculatorService{


    private final IOService ioService;

    public CalculatorServiceImpl(IOService ioService) {
        this.ioService = ioService;
    }


    @Override
    public void readTwoDigitsAndMultiply() {
        int d1 = Integer.parseInt(ioService.readString());// read 2 digits (line by line)
        int d2 = Integer.parseInt(ioService.readString());// read 2 digits (line by line)
        multiplyAndOutResult(d1, d2);                     // multiplies and outputs the result
    }
    private void multiplyAndOutResult(int d1, int d2) {
        ioService.out(String.format("%d * %d = %d", d1, d2, d1 * d2));
    }

    @Override
    public void readTwoDigitsAndDivide() {
        int d1 = Integer.parseInt(ioService.readString());// read 2 digits (line by line)
        int d2 = Integer.parseInt(ioService.readString());// read 2 digits (line by line)
        divideAndOutResult(d1, d2);                     // divides and outputs the result
    }
    private void divideAndOutResult(int d1, int d2) {
        ioService.out(String.format("%d / %d = %d", d1, d2, d1 / d2));
    }

    @Override
    public void readTwoDigitsAndSubtract() {
        int d1 = Integer.parseInt(ioService.readString());// read 2 digits (line by line)
        int d2 = Integer.parseInt(ioService.readString());// read 2 digits (line by line)
        subtractAndOutResult(d1, d2);                     // subtracts and outputs the result
    }
    private void subtractAndOutResult(int d1, int d2) {
        ioService.out(String.format("%d - %d = %d", d1, d2, d1 - d2));
    }

    @Override
    public void readTwoDigitsAndSum() {
        int d1 = Integer.parseInt(ioService.readString());// read 2 digits (line by line)
        int d2 = Integer.parseInt(ioService.readString());// read 2 digits (line by line)
        sumAndOutResult(d1, d2);                     // adds and outputs the result
    }
    private void sumAndOutResult(int d1, int d2) {
        ioService.out(String.format("%d + %d = %d", d1, d2, d1 + d2));
    }
}
