import org.testng.annotations.*;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.testng.Assert.*;


public class ConsoleTest {

    private static final String EOL = System.lineSeparator();
    private static final String TEXT_TO_PRINT1 = "Checking the console 1";
    private static final String TEXT_TO_PRINT2 = "Checking the console 2";

    private PrintStream backup;
    private ByteArrayOutputStream bos;
    private IOService ioService;

    @BeforeMethod // system out we are transferring it to the test state
    public void setUp() {
        System.out.println("Preparation before tests");
        System.out.println(Thread.currentThread().getName());
        backup = System.out;
        bos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(bos));
        ioService = new Console();
    }


    @AfterMethod //system out в привычное состояние
    public void tearDown() {
        System.setOut(backup);
        System.out.println("We clean up after the tests");
    }

    @Test
    void shouldPrintOnlyFirstCreedLine() throws InterruptedException {
        ioService.out(TEXT_TO_PRINT1);
        Thread.sleep(1000);
        assertEquals((bos.toString()),(TEXT_TO_PRINT1 + EOL),"Line 1 is not equal to");

    }

    @Test
    void shouldPrintOnlySecondCreedLine() {
        ioService.out(TEXT_TO_PRINT2);
        assertEquals((bos.toString()),(TEXT_TO_PRINT2 + EOL), "Line 2 is not equal to");
    }
}