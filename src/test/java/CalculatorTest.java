import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import org.mockito.InOrder;


import org.mockito.Mockito;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import static org.testng.Assert.assertThrows;


public class CalculatorTest {

    private InOrder inOrder;
    private IOService ioService;
    private CalculatorService calculatorService;

    @DataProvider
    public Object [][] testEqualsSum(){
        return new Object[][]{
                {"11","11","11 + 11 = 22"},
                {"-11","11","-11 + 11 = 0"},
                {"1000","1000000","1000 + 1000000 = 1001000"}//,
                //{"11","11","11 + 11 = 222222"} - the test crashes
        };
    }
    @DataProvider
    public Object [][] testEqualsMultiply(){
        return new Object[][]{
                {"53","61","53 * 61 = 3233"},
                {"0","61","0 * 61 = 0"},
                {"-10","61","-10 * 61 = -610"},
                {"100000","61","100000 * 61 = 6100000"}//,
                //{"53","61","53 * 61 = 333333"} - the test crashes
        };
    }
    @DataProvider
    public Object [][] testEqualsDivide(){
        return new Object[][]{
                {"63","7","63 / 7 = 9"},
                {"0","63","0 / 63 = 0"},
                {"63","-7","63 / -7 = -9"},
                {"1000000","10","1000000 / 10 = 100000"}//,
                //{"63","7","63 / 7 = 999999"} - тест падает
                //{"63","0","63 / 0 = ArithmeticException"} - the test crashes
        };
    }
    @DataProvider
    public Object [][] testEqualsSubtract(){
        return new Object[][]{
                {"53","6","53 - 6 = 47"},
                {"0","6","0 - 6 = -6"},
                {"-9","7","-9 - 7 = -16"},
                {"1000000","10","1000000 - 10 = 999990"},
                //{"53","6","53 - 6 = 444444"} - the test crashes
        };
    }


    @DataProvider
    public Object [][] testNumberFormatException(){
        return new Object[][]{
                {"53","61"},
                //{53,61} - the test crashes
        };
    }

    @BeforeMethod // we prepare the I/O service, substitute the interface plug
    public void setUp() {
        ioService = Mockito.mock(IOService.class);
        calculatorService = new CalculatorServiceImpl(ioService);
        inOrder = inOrder(ioService); // on the basis of this object begins to record actions on it
    }

    // read two numbers, multiply them and output the result
    @Test(dataProvider = "testEqualsMultiply")
    void shouldReadTwoDigitsMultiplyAndOutputResultWithIOService(String FIRST_DIGIT,String SECOND_DIGIT,
                                                                 String OUTPUT_RESULT) {
        // the initial condition (if reading a string), then returns the values in attempts to read the lines
        given(ioService.readString()).willReturn(FIRST_DIGIT).willReturn(SECOND_DIGIT);

        // calling multiplication
        calculatorService.readTwoDigitsAndMultiply();

        // we immediately check the order of function calls and the result
        inOrder.verify(ioService, times(2)).readString();
        inOrder.verify(ioService, times(1)).out(OUTPUT_RESULT);
    }

    // прочитать два числа, перемножить их и вывести результат
    @Test(dataProvider = "testEqualsSum")
    void shouldReadTwoDigitsSumAndOutputResultWithIOService(String FIRST_DIGIT,String SECOND_DIGIT,
                                                            String OUTPUT_RESULT) {
        // the initial condition (if reading a string), then returns the values in attempts to read the lines
        given(ioService.readString()).willReturn(FIRST_DIGIT).willReturn(SECOND_DIGIT);

        // calling multiplication
        calculatorService.readTwoDigitsAndSum();

        // we immediately check the order of function calls and the result
        inOrder.verify(ioService, times(2)).readString();
        inOrder.verify(ioService, times(1)).out(OUTPUT_RESULT);
    }

    // read two numbers, multiply them and output the result
    @Test(dataProvider = "testEqualsDivide")
    void shouldReadTwoDigitsDivideAndOutputResultWithIOService(String FIRST_DIGIT,String SECOND_DIGIT,
                                                               String OUTPUT_RESULT) {
        // the initial condition (if reading a string), then returns the values in attempts to read the lines
        given(ioService.readString()).willReturn(FIRST_DIGIT).willReturn(SECOND_DIGIT);

        // calling multiplication
        calculatorService.readTwoDigitsAndDivide();

        // we immediately check the order of function calls and the result
        inOrder.verify(ioService, times(2)).readString();
        inOrder.verify(ioService, times(1)).out(OUTPUT_RESULT);
    }
    // read two numbers, multiply them and output the result
    @Test(dataProvider = "testEqualsSubtract")
    void shouldReadTwoDigitsSubtractAndOutputResultWithIOService(String FIRST_DIGIT,String SECOND_DIGIT,
                                                                 String OUTPUT_RESULT) {
        // the initial condition (if reading a string), then returns the values in attempts to read the lines
        given(ioService.readString()).willReturn(FIRST_DIGIT).willReturn(SECOND_DIGIT);

        // calling multiplication
        calculatorService.readTwoDigitsAndSubtract();

        // we immediately check the order of calling functions and resultsт
        inOrder.verify(ioService, times(2)).readString();
        inOrder.verify(ioService, times(1)).out(OUTPUT_RESULT);
    }



    //readTwoDigitsAndMultiply throw NumberFormatException if a string is entered instead of a number
    @Test(dataProvider = "testNumberFormatException")
    void shouldThrowNumberFormatExceptionWhenEnteredIsNotANumber_readTwoDigitsAndMultiply(String FIRST_DIGIT,String SECOND_DIGIT) {
        given(ioService.readString()).willReturn("").willReturn(FIRST_DIGIT);
        given(ioService.readString()).willReturn("").willReturn(SECOND_DIGIT);

        assertThrows(NumberFormatException.class, () -> calculatorService.readTwoDigitsAndMultiply());
    }

    //readTwoDigitsAndSum Throw NumberFormatException if a string is entered instead of a number
    @Test(dataProvider = "testNumberFormatException")
    void shouldThrowNumberFormatExceptionWhenEnteredIsNotANumber_readTwoDigitsAndSum(String FIRST_DIGIT,String SECOND_DIGIT) {
        given(ioService.readString()).willReturn("").willReturn(FIRST_DIGIT);
        given(ioService.readString()).willReturn("").willReturn(SECOND_DIGIT);

        assertThrows(NumberFormatException.class, () -> calculatorService.readTwoDigitsAndSum());
    }
    //readTwoDigitsAndSubtract throw NumberFormatException if a string was entered instead of a number
    @Test(dataProvider = "testNumberFormatException")
    void shouldThrowNumberFormatExceptionWhenEnteredIsNotANumber_readTwoDigitsAndSubtract(String FIRST_DIGIT,String SECOND_DIGIT) {
        given(ioService.readString()).willReturn("").willReturn(FIRST_DIGIT);
        given(ioService.readString()).willReturn("").willReturn(SECOND_DIGIT);

        assertThrows(NumberFormatException.class, () -> calculatorService.readTwoDigitsAndSubtract());
    }
    //readTwoDigitsAndDivide throw NumberFormatException if a string was entered instead of a number
    @Test(dataProvider = "testNumberFormatException")
    void shouldThrowNumberFormatExceptionWhenEnteredIsNotANumber_readTwoDigitsAndDivide(String FIRST_DIGIT,String SECOND_DIGIT) {
        given(ioService.readString()).willReturn("").willReturn(FIRST_DIGIT);
        given(ioService.readString()).willReturn("").willReturn(SECOND_DIGIT);

        assertThrows(NumberFormatException.class, () -> calculatorService.readTwoDigitsAndDivide());
    }
}
